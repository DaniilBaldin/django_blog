from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .forms import PostForm

from .models import Post, Category


def post_list(request):
    posts = Post.objects.filter(post_time__lte=timezone.now()).order_by('post_time')
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post.html', {'post': post})


def post(request):
    Post.objects.all().delete()
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.post_time = timezone.now()
            post.save()
        return redirect('post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/edit_post.html', {'form': form})


def category_list(request, pk):
    category = get_object_or_404(Category, pk=pk)
    category_post = Post.objects.filter(category=category.id).order_by('post_time')
    context = {'category': category, 'category_post': category_post}
    return render(request, 'blog/category.html', context=context)
