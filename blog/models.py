from datetime import date

from django.db import models

# Create your models here.


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Author(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Post(models.Model):
    title = models.CharField(max_length=100)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    text = models.TextField()
    author = models.ForeignKey(Author, on_delete=models.CASCADE)
    post_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.title}----{self.post_time}'

    class Meta:
        ordering = ['title']

